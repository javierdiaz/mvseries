package com.mv.series;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;

import com.google.gson.Gson;
import com.mv.series.jsonclasses.Episodes;
import com.mv.series.jsonclasses.Series;

public class SemanaSeries {
	
	public static HashMap<String, String> KNOWN_SERIES = new HashMap<>();//Serie;Canal
	public static HashMap<String, String> CHANNEL_IMAGES = new HashMap<>();//Canal;Imagen
	public static HashMap<String, String> SHOW_THREADS = new HashMap<>();//Serie;Hilo en mv
	
	public static String[] dias = {"Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"};
	
	public static void main(String[] args) throws Exception {
		File file = new File("hilo"+System.currentTimeMillis()+".txt");
		FileOutputStream fos = new FileOutputStream(file);
		initSeries();
		
		//creamos un calendar y nos movemos al domingo para poder tener las series de esa semana
		Calendar calendar = Calendar.getInstance();
		int days = Calendar.SUNDAY - calendar.get(Calendar.DAY_OF_WEEK);  
		if (days < 0) {  days += 7; }  
		calendar.add(Calendar.DAY_OF_YEAR, days);
		Date date = new Date(calendar.getTimeInMillis());
		SimpleDateFormat format = new SimpleDateFormat("YMMdd");
		
		//creamos la url y la leemos
		String ur = "http://api.trakt.tv/calendar/shows.json/d474f94625838bfdf23f4c29c00c5ef9/"+format.format(date)+"/7"; //7 es el n�mero de dias
		URL url = new URL(ur);
		Scanner scanner = new Scanner(url.openStream());
		scanner.useDelimiter("\\A");
		Series[] lista = new Gson().fromJson(scanner.next(), Series[].class);
		scanner.close();
		
		int i=0;
		for (Series day : lista) {
			Calendar fecha = Calendar.getInstance();
			fecha.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(day.getDate()));
			fecha.add(Calendar.DAY_OF_MONTH, 1);
			fos.write(("\n\n[b]"+dias[i++]+" "+fecha.get(Calendar.DAY_OF_MONTH)+"[/b]\n\n").getBytes());
			Collections.sort(day.getEpisodes(), new Comparator<Episodes>() { //se ordenan las series por orden alfab�tico
				public int compare(Episodes o1, Episodes o2) {
					return o1.getShow().getTitle().compareTo(o2.getShow().getTitle());
				};
			});
			for (int j=0; j<day.getEpisodes().size(); j++) {
				Episodes episode = day.getEpisodes().get(j);
				System.out.println(episode.getShow().getTitle()+": "+episode.getEpisode().getTitle());
				if (KNOWN_SERIES.containsKey(episode.getShow().getTitle())) {
					fos.write(formatEpisode(
							episode.getShow().getNetwork(),
							episode.getShow().getTitle(), 
							episode.getEpisode().getTitle(),
							episode.getEpisode().getSeason().intValue(),
							episode.getEpisode().getNumber().intValue()).getBytes());
				}
			}
		}
		fos.close();
	}
	
	private static String formatEpisode(String canal, String serie, String nombreEpisodio, int season, int number) {
		String formato = "[img]%s[/img] [url=\"%s\"]%s[/url] S%02dE%02d - %s\n";
		if (number == 1) { formato = "[b]"+formato+"[/b]"; }
		if (CHANNEL_IMAGES.get(canal) == null) {
			System.out.println("Falta la imagen para: "+canal);
		}
		if (SHOW_THREADS.get(serie) == null) {
			System.out.println("Falta el hilo para: "+serie);
		}
		return String.format(formato, CHANNEL_IMAGES.get(canal), SHOW_THREADS.get(serie), serie, season, number, nombreEpisodio);
	}

	private static void initSeries() throws Exception {
		Scanner scanner = new Scanner(new File("series.txt"));
		while (scanner.hasNextLine()) { //leemos series.txt que es del tipo nombreSerie;canal;hiloEnMV
			String[] line = scanner.nextLine().split(";");
			KNOWN_SERIES.put(line[0], line[1]);
			SHOW_THREADS.put(line[0], line[2]);
		}
		scanner.close();
		
		Scanner scanner2 = new Scanner(new File("canales.txt"));
		while (scanner2.hasNextLine()) { //leemos canales.txt que es del tipo canal;urlImagen
			String[] line = scanner2.nextLine().split(";");
			CHANNEL_IMAGES.put(line[0], line[1]);
		}
		scanner2.close();
	}
}

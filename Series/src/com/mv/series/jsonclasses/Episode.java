
package com.mv.series.jsonclasses;


public class Episode{
   	private Number first_aired;
   	private String first_aired_iso;
   	private Number first_aired_localized;
   	private Number first_aired_utc;
   	private Images images;
   	private Number number;
   	private String overview;
   	private Ratings ratings;
   	private Number season;
   	private String title;
   	private String url;

 	public Number getFirst_aired(){
		return this.first_aired;
	}
	public void setFirst_aired(Number first_aired){
		this.first_aired = first_aired;
	}
 	public String getFirst_aired_iso(){
		return this.first_aired_iso;
	}
	public void setFirst_aired_iso(String first_aired_iso){
		this.first_aired_iso = first_aired_iso;
	}
 	public Number getFirst_aired_localized(){
		return this.first_aired_localized;
	}
	public void setFirst_aired_localized(Number first_aired_localized){
		this.first_aired_localized = first_aired_localized;
	}
 	public Number getFirst_aired_utc(){
		return this.first_aired_utc;
	}
	public void setFirst_aired_utc(Number first_aired_utc){
		this.first_aired_utc = first_aired_utc;
	}
 	public Images getImages(){
		return this.images;
	}
	public void setImages(Images images){
		this.images = images;
	}
 	public Number getNumber(){
		return this.number;
	}
	public void setNumber(Number number){
		this.number = number;
	}
 	public String getOverview(){
		return this.overview;
	}
	public void setOverview(String overview){
		this.overview = overview;
	}
 	public Ratings getRatings(){
		return this.ratings;
	}
	public void setRatings(Ratings ratings){
		this.ratings = ratings;
	}
 	public Number getSeason(){
		return this.season;
	}
	public void setSeason(Number season){
		this.season = season;
	}
 	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
 	public String getUrl(){
		return this.url;
	}
	public void setUrl(String url){
		this.url = url;
	}
}

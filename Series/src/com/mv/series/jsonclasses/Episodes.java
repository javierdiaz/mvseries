
package com.mv.series.jsonclasses;


public class Episodes{
   	private Episode episode;
   	private Show show;

 	public Episode getEpisode(){
		return this.episode;
	}
	public void setEpisode(Episode episode){
		this.episode = episode;
	}
 	public Show getShow(){
		return this.show;
	}
	public void setShow(Show show){
		this.show = show;
	}
}

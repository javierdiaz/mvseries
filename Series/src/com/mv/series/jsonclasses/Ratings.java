
package com.mv.series.jsonclasses;


public class Ratings{
   	private Number hated;
   	private Number loved;
   	private Number percentage;
   	private Number votes;

 	public Number getHated(){
		return this.hated;
	}
	public void setHated(Number hated){
		this.hated = hated;
	}
 	public Number getLoved(){
		return this.loved;
	}
	public void setLoved(Number loved){
		this.loved = loved;
	}
 	public Number getPercentage(){
		return this.percentage;
	}
	public void setPercentage(Number percentage){
		this.percentage = percentage;
	}
 	public Number getVotes(){
		return this.votes;
	}
	public void setVotes(Number votes){
		this.votes = votes;
	}
}

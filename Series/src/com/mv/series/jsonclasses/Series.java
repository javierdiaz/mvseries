
package com.mv.series.jsonclasses;

import java.util.ArrayList;

public class Series {
   	private String date;
   	private ArrayList<Episodes> episodes;

 	public String getDate(){
		return this.date;
	}
	public void setDate(String date){
		this.date = date;
	}
 	public ArrayList<Episodes> getEpisodes(){
		return this.episodes;
	}
	public void setEpisodes(ArrayList<Episodes> episodes){
		this.episodes = episodes;
	}
}


package com.mv.series.jsonclasses;


public class Show {
	
   	private String air_day;
   	private String air_day_localized;
   	private String air_day_utc;
   	private String air_time;
   	private String air_time_localized;
   	private String air_time_utc;
   	private String certification;
   	private String country;
   	private Number first_aired;
   	private String first_aired_iso;
   	private Number first_aired_localized;
   	private Number first_aired_utc;
   	private Images images;
   	private String imdb_id;
   	private String network;
   	private String overview;
   	private Ratings ratings;
   	private Number runtime;
   	private String title;
   	private String tvdb_id;
   	private String tvrage_id;
   	private String url;
   	private Number year;

 	public String getAir_day(){
		return this.air_day;
	}
	public void setAir_day(String air_day){
		this.air_day = air_day;
	}
 	public String getAir_day_localized(){
		return this.air_day_localized;
	}
	public void setAir_day_localized(String air_day_localized){
		this.air_day_localized = air_day_localized;
	}
 	public String getAir_day_utc(){
		return this.air_day_utc;
	}
	public void setAir_day_utc(String air_day_utc){
		this.air_day_utc = air_day_utc;
	}
 	public String getAir_time(){
		return this.air_time;
	}
	public void setAir_time(String air_time){
		this.air_time = air_time;
	}
 	public String getAir_time_localized(){
		return this.air_time_localized;
	}
	public void setAir_time_localized(String air_time_localized){
		this.air_time_localized = air_time_localized;
	}
 	public String getAir_time_utc(){
		return this.air_time_utc;
	}
	public void setAir_time_utc(String air_time_utc){
		this.air_time_utc = air_time_utc;
	}
 	public String getCertification(){
		return this.certification;
	}
	public void setCertification(String certification){
		this.certification = certification;
	}
 	public String getCountry(){
		return this.country;
	}
	public void setCountry(String country){
		this.country = country;
	}
 	public Number getFirst_aired(){
		return this.first_aired;
	}
	public void setFirst_aired(Number first_aired){
		this.first_aired = first_aired;
	}
 	public String getFirst_aired_iso(){
		return this.first_aired_iso;
	}
	public void setFirst_aired_iso(String first_aired_iso){
		this.first_aired_iso = first_aired_iso;
	}
 	public Number getFirst_aired_localized(){
		return this.first_aired_localized;
	}
	public void setFirst_aired_localized(Number first_aired_localized){
		this.first_aired_localized = first_aired_localized;
	}
 	public Number getFirst_aired_utc(){
		return this.first_aired_utc;
	}
	public void setFirst_aired_utc(Number first_aired_utc){
		this.first_aired_utc = first_aired_utc;
	}
 	public Images getImages(){
		return this.images;
	}
	public void setImages(Images images){
		this.images = images;
	}
 	public String getImdb_id(){
		return this.imdb_id;
	}
	public void setImdb_id(String imdb_id){
		this.imdb_id = imdb_id;
	}
 	public String getNetwork(){
		return this.network;
	}
	public void setNetwork(String network){
		this.network = network;
	}
 	public String getOverview(){
		return this.overview;
	}
	public void setOverview(String overview){
		this.overview = overview;
	}
 	public Ratings getRatings(){
		return this.ratings;
	}
	public void setRatings(Ratings ratings){
		this.ratings = ratings;
	}
 	public Number getRuntime(){
		return this.runtime;
	}
	public void setRuntime(Number runtime){
		this.runtime = runtime;
	}
 	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
 	public String getTvdb_id(){
		return this.tvdb_id;
	}
	public void setTvdb_id(String tvdb_id){
		this.tvdb_id = tvdb_id;
	}
 	public String getTvrage_id(){
		return this.tvrage_id;
	}
	public void setTvrage_id(String tvrage_id){
		this.tvrage_id = tvrage_id;
	}
 	public String getUrl(){
		return this.url;
	}
	public void setUrl(String url){
		this.url = url;
	}
 	public Number getYear(){
		return this.year;
	}
	public void setYear(Number year){
		this.year = year;
	}
}
